# Kairos DevOps exercise with CI/CD

    Making use of Docker (dockerfile & docker-compose) create a deployable envrironment that englobes 3 different apps.

## CI/CD

    Sonarcloud is being used as CI/CD pipeline

## Tech:
    Technologies used on this exercise are: 

    - docker (dockerfile & dockercompose)
    - mysql
    - angular
    - nodejs
    - nginx
    - typescript
    - yml
    - SonarCloud